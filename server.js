const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = 3000;

app.use(bodyParser.json());

// Sumar n1 + n2
app.get('/results/:n1/:n2', (req, res) => {
  const n1 = parseInt(req.params.n1);
  const n2 = parseInt(req.params.n2);
  const resultado = n1 + n2;
  res.json({ resultado });
});

// Multiplicar n1 * n2
app.post('/results/', (req, res) => {
  const n1 = parseInt(req.body.n1);
  const n2 = parseInt(req.body.n2);
  const resultado = n1 * n2;
  res.json({ resultado });
});

// Dividir n1 / n2
app.put('/results/', (req, res) => {
  const n1 = parseInt(req.body.n1);
  const n2 = parseInt(req.body.n2);
  const resultado = n1 / n2;
  res.json({ resultado });
});

// Potencia n1 ^ n2
app.patch('/results/', (req, res) => {
  const n1 = parseInt(req.body.n1);
  const n2 = parseInt(req.body.n2);
  const resultado = Math.pow(n1, n2);
  res.json({ resultado });
});

// Restar n1 - n2
app.delete('/results/:n1/:n2', (req, res) => {
  const n1 = parseInt(req.params.n1);
  const n2 = parseInt(req.params.n2);
  const resultado = n1 - n2;
  res.json({ resultado });
});

app.listen(PORT, () => {
  console.log(`API REST ejecutándose en http://localhost:${PORT}`);
});
